unit zlib_cl;

{$MODE DELPHI}
{$codepage UTF8}

interface

uses
  zstream,
  classes,
  sysutils,
  v8napi;

type
  TZlib1C = class(TV8UserObject)
  private
    fSkipHeader : boolean;
  public
    function decompress(RetValue: PV8Variant; Params: PV8ParamArray; const ParamCount: integer): boolean;
    function donothing(RetValue: PV8Variant; Params: PV8ParamArray; const ParamCount: integer): boolean;
    constructor Create; override;
  end;

implementation

function TZlib1C.decompress(RetValue: PV8Variant; Params: PV8ParamArray; const ParamCount: integer): boolean;
var
  filesource : String;
  filedest   : String;
  vdecompres : TDecompressionStream;
  vsourcestr : TFileStream;
  vdeststr   : TfileStream;
  B   : array[1..2048] of byte;
  cnt : Integer;

begin
  filesource := V8AsAString(@Params[1]);
  filedest   := V8AsAString(@Params[2]);
  if not FileExists(filesource) then begin
    V8SetWString(RetValue, '');
    result := true;
    exit;
  end;
  vsourcestr := TFileStream.Create(filesource, fmOpenRead);
  try
    vdecompres := TDecompressionStream.Create(vsourcestr);
    try
      vdeststr := TFileStream.Create(filedest, fmCreate);
      try
        repeat
           cnt :=  vdecompres.Read(B, sizeof(B));
           if cnt > 0 then vdeststr.write(B, cnt);
        until cnt < sizeof(B);
      finally
        vdeststr.Free;
      end;
    finally
      vdecompres.Free;
    end;
  finally
    vsourcestr.Free;
  end;
  V8SetWString(RetValue, 'OK');
  result := true;
end;

function TZlib1C.donothing(RetValue: PV8Variant; Params: PV8ParamArray; const ParamCount: integer): boolean;
begin
  V8SetWString(RetValue, 'Hello, World');
  result := true;
end;

constructor TZlib1C.Create;
begin
  fSkipHeader := true;
end;

begin
  with ClassRegList.RegisterClass(TZlib1C, 'ZlibCompression', 'TZlib1C') do begin
    AddFunc('decompress', 'распаковать', @TZlib1C.decompress, 2);
    AddFunc('donothing', ',бездельничать', @TZlib1C.donothing, 0);
  end;

end.
